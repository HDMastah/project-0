  #include <Wire.h> 
  const int latchPin = 12;  //Pin connected to ST_CP of 74HC595
  const int clockPin = 8;   //Pin connected to SH_CP of 74HC595 
  const int dataPin = 11;   //Pin connected to DS of 74HC595 
  int gewenst = 1;          //waar moet de lift stoppen (Master arduino houdt dit bij
  int geroepen;             //op welke etage wordt de lift geroepen, 0 = begane grond, -1 = nergens.
  int huidig = 1;           //op welke verdieping de lift is (Master arduino houdt dit bij)
  bool deuren;              //of de liftdeuren open moeten of niet (eigenlijk niet nodig, maarja)
  bool bewegen;             //of de lift moet bewegen
  int IR = 2;               //infrarood Sensor aan pin 2
  int knopup = 3;           //knop aan pin 3
  int knopdown = 4;         //knop aan pin 4
  int knopupled = 5;        //knop LED aan pin 5
  int knopdownled = 6;      //knop LED aan pin 6
  int liftlamp = 7;         //rode LED die aangeeft of de lift is aangekomen
void setup() {
  pinMode (IR, INPUT);
  pinMode (knopup, INPUT_PULLUP);   //stel op de knop pinnen de interne weerstand aan, en stel ze in als input
  pinMode (knopdown, INPUT_PULLUP);
  pinMode (knopupled, OUTPUT);
  pinMode (knopdownled, OUTPUT);
  pinMode (liftlamp, OUTPUT);
  pinMode (latchPin,OUTPUT);
  pinMode (clockPin, OUTPUT);
  pinMode (dataPin, OUTPUT);
  Serial.begin(9600); 
  digitalWrite(latchPin, LOW);              // test waarde instellen op de segment display
  shiftOut(dataPin, clockPin,MSBFIRST,73);  //
  digitalWrite(latchPin, HIGH);             //
}

void loop() {

  if (digitalRead(IR)== 0) {  //kijk of de lift op deze etage is. zo ja, zet de display op deze etage
    huidig = 0;     // doorgeven aan de Master arduino op welke etage de lift zich bevindt.
    Serial.println("de lift is nu op de begane grond");
    digitalWrite(latchPin, LOW);            //
    shiftOut(dataPin, clockPin,MSBFIRST,3); //schrijf '0' naar de segment display
    digitalWrite(latchPin, HIGH);           //
  }
  if (digitalRead(IR) == 0 && gewenst == 0) { //controleren of de lift er is enof de lift op deze etage moet stoppen.
    bewegen = false;                          //zo ja, dan moet de lift stoppen, liftlampje aan en de knoppen uit (en deuren open).
    deuren = true;
    Serial.println("de lift is gearriveerd op de begane grond");
    digitalWrite (liftlamp, HIGH);
    digitalWrite (knopupled, LOW); //knop LED's uit zetten
    digitalWrite (knopdownled, LOW);
    delay (10000); // 10 seconden wachten voordat de lift weer iets mag doen.
    deuren = false;
    gewenst = -1;  //test waarde, verwijderen bij intergratie.
    digitalWrite (liftlamp, LOW);
  } 
  if (digitalRead(knopup) == 0){ //Nts: Riching moet ook doorgegeven worden.
    geroepen = 0;     //geef door aan de Master welke etage geroepen is.
    if (huidig <= 0){ //testcommando, Master arduino regelt dit bij intergratie.
      gewenst = 0; 
    }
    Serial.println("knopup op de begane grond is ingedrukt");
  }
    if (digitalRead(knopdown) == 0){ //Nts: Riching moet ook doorgegeven worden.
    geroepen = 0;     //geef door aan de Master welke etage geroepen is.
    if (huidig >= 0){ //testcommando, Master arduino regelt dit bij intergratie.
      gewenst = 0; 
    }
    Serial.println("knopdown op de begane grond is ingedrukt");
  }
  
  
}
